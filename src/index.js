import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
function Products (){
    return(
        <table> 
            <thead>
                <tr>
                    <td>Назва зодіаку</td>
                    <td>Дата</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Козеріг</td>
                    <td>22 грудня – 20 січня</td>
                </tr>
                <tr>
                    <td>Водолій</td>
                    <td>21 січня – 24 лютого</td>
                </tr>
                <tr>
                    <td>Риби</td>
                    <td>21 лютого – 20 березня</td>
                </tr>
                <tr>
                    <td>Овен</td>
                    <td>21 березня – 20 квітня</td>
                </tr>
                <tr>
                    <td>Телець</td>
                    <td>21 квітня – 20 травня</td>
                </tr>
                <tr>
                    <td>Близнята</td>
                    <td> 21 травня – 21 червня</td>
                </tr>
                <tr>
                    <td>Рак</td>
                    <td>22 червня – 22 липня</td>
                </tr>
                <tr>
                    <td>Лев</td>
                    <td>23 липня – 23 серпня</td>
                </tr>
                <tr>
                    <td>Діва</td>
                    <td>24 серпня – 23 вересня</td>
                </tr>
                <tr>
                    <td>Терези</td>
                    <td>24 вересня – 23 жовтня</td>
                </tr>
                <tr>
                    <td>Скорпіон</td>
                    <td>24 жовтня – 22 листопада</td>
                </tr>
                <tr>
                    <td>Стрілець</td>
                    <td>23 листопада – 21 грудня</td>
                </tr>
            </tbody>
        </table>
    )
}
ReactDOM.render(<Products></Products>, document.querySelector(".one"))